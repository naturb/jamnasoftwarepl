import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Idosell from '../components/Idosell';
import Services from '../components/tabs/Services';
import Showcase from '../components/tabs/Showcase';
import Jobs from '../components/tabs/Jobs';
import Contact from '../components/tabs/Contact';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/idosell-shop',
    name: 'Idosell',
    component: Idosell
  },
  {
    path: '/services',
    name: 'Services',
    component: Services
  },
  {
    path: '/showcase',
    name: 'Showcase',
    component: Showcase
  },
  {
    path: '/jobs',
    name: 'Jobs',
    component: Jobs
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  }
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
});

export default router;
