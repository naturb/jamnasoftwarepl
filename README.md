# jamna-website-vue

## Requirements
* nodejs
* vue-cli

## Installation
```
$: npm install -g @vue/cli
$: git clone <url>
$: cd jamnasoftwarepl
$: npm install
$: npm run serve
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

## Obfuscator
```
$: javascript-obfuscator ./dist/js --output ./dist/obfuscated [options]
$: cd dist/js
$: rm *.js
$: cd ../obfuscated/dist/js
$: cp * ../../../js 
$: cd ../../../
$: rm -rf obfuscated

```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
