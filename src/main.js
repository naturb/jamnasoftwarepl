import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import App from './App.vue';
import router from './router';
import VueParticles from 'vue-particles';
import VueKinesis from 'vue-kinesis';
import VueMq from 'vue-mq';
import VueMeta from 'vue-meta';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/js/all.js';


Vue.use(VueKinesis);
Vue.use(VueParticles);
Vue.use(BootstrapVue);
Vue.use(VueMeta);

Vue.use(VueMq, {
  breakpoints: { 
    xs: 450,
    sm: 600,
    md: 992,
    bg: 1250,
    lg: Infinity,
  },
  defaultBreakpoint: 'lg'
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
